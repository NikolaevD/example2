import controller.Controller;
import view.View;
import model.Model;

public class App {
    public static void main(String[] args) {
        Model model = new Model();
        System.out.println("Model created");
        View view = new View();
        System.out.println("View created");
        Controller controller = new Controller();
    }
}